import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Api } from '../provider/api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(signUp: FormBuilder, public router: Router, public api: Api) {
    this.registerForm = signUp.group({
      'name': [null, Validators.compose([Validators.required])],
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'phone': [null, Validators.compose([Validators.required])],
      // 'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      // 'confirmPassword': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    });
   }

  ngOnInit() {
  }
  onFormSubmit(valid, e) {
    // console.log(e);
    let data = {
      Name: this.registerForm.get('name').value,
      Email: this.registerForm.get('email').value,
      Phone: this.registerForm.get('phone').value
    };


    //  this.api.getUsers().subscribe(response=>{
    //   console.log(response);
    //  }, (err)=>{
    //      console.log(err);
    //    });
    this.api.register(data).subscribe(response=>{
        console.log(response);
    }, (err)=>{
      console.log(err);
    });
 }

 getUsers(){
  this.router.navigate(['profile']);
 }

}
