import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Api } from '../provider/api';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  registerForm: FormGroup;
  items: number[] = [1];
  users : any;
  userdata: String;
  constructor(signUp: FormBuilder, public router: Router,public api: Api) { 
    this.registerForm = signUp.group({
      'name': [null, Validators.compose([Validators.required])],
      'email': [null, Validators.compose([Validators.required])],
      'number': [null, Validators.compose([Validators.required])]
    });
    this.getAllUsers();
  }


getAllUsers(){
    this.api.getUsers().subscribe(response=>{
      console.log(response);
      this.users = response;
      console.log( this.users );
     }, (err)=>{
       //  console.log(err);
       });
}

getData(evt){
 
    console.log(this.registerForm.get('email').value);
   // console.log("sdv");
}

  ngOnInit() {
  }
  addRow(){
    this.items.push(this.items.length + 1)
  }
  removeRow(index) {
    this.items.splice(index,1);
  }
  onFormSubmit() {
    console.log("form submit");
  }
  addUsers(){
    this.router.navigate(['register']);
  }
}
