import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable()
export class Api {
    apiKey: String = "http://localhost:60912/";
    constructor(public httpClint: HttpClient) {
        
    }

    register(data){

        let headers = new HttpHeaders();
        headers = headers.append('Access-Control-Allow-Origin', '*');
        headers = headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
       // headers =  headers.append("content-type", "application/json");
      

       //  console.log(data);
        return this.httpClint.get(this.apiKey + 'api/user/saveusers?name='+data.Name
        +'&email='+data.Email
        +'&phone='+data.Phone);
    }

    getUsers(){
      
        return this.httpClint.get(this.apiKey + 'api/user/getusers');
    }
}